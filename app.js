'use strict';

const noble = require('noble');
const serviceuuid = `0179bbd0535148b5bf6d2167639bc867`;
const charauuid = `0179bbd1535148b5bf6d2167639bc867`;
var fs = require("fs");

var text = "テスト出力";
//キャラクタリスティックにアクセスしてデータやりとり
const accessChara = (chara) => {
    console.log('-----Start GATT Access-----')
    chara.notify(true, (err) => {
        if (err) {
          console.log('listen notif error', err)
        } else {
          console.log('listen notif')
        }
    });
   chara.on('data', (data, isNotif) => {
//        const jsonStr = data.toString('utf-8');
//        const jsonData = JSON.parse(jsonStr);
        console.log("data" + data);
        fs.appendFile('out.txt', data);
    });

//for (var i = 0; i < chara.length; i ++) {
//	chara[i].on("data", function (data, isNotification) {
//		console.log("data: " + data);
//	});
//	chara[i].subscribe();
//}
}


//discovered BLE device
const discovered = (peripheral) => {
    console.log(`BLE Device Found: ${peripheral.advertisement.localName}(${peripheral.uuid}) RSSI${peripheral.rssi}`);

    if(peripheral.advertisement.localName === 'LapisDev'){
        noble.stopScanning();
        console.log('device found');
        console.log(`service discover...`);

        peripheral.connect(error => {
            if (error) {
                console.log("connection error:", error)
            } else {
                console.log("device connected");
            }

            peripheral.discoverServices([],(err, services) => {
                if (error) {
                    console.log("discover service error", error)
                }
                console.log('discover service');               
                services.forEach(service => {
                    if(service.uuid === serviceuuid){
                        service.discoverCharacteristics([], (error, charas) => {
                            console.log('discover chara');
                            charas.forEach(chara => {
                                if(chara.uuid === charauuid){
                                    console.log("found chara: ", chara.uuid)
                                    accessChara(chara);
                                }
                            });
                        });
                    }
                });
            });
        });
    }
}

//BLE scan start
const scanStart = () => {
    noble.startScanning();
    noble.on('discover', discovered);
}

if(noble.state === 'poweredOn'){
    scanStart();
}else{
    noble.on('stateChange', scanStart);
}
function bytes2str(data){
	var str = "";
	for (var i = 0; i < data.length; i++){
		str += ('00'  + (data[i].toString(16))).slice( -2 );
		}
	return str ; 
}
